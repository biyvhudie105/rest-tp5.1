Dawn-Api (thinkPHP5.1适配)
===============

## 说明
thinkphp5编写的restful风格的API，集API请求处理，权限认证，自动生成文档等功能；

 - restful风格处理请求
 > 每个接口对于一个控制器，method对应[method]方法响应

 - 权限认证
 > Basic,Oauth Client Credentials Grant
 
 - 文档生成
 > 简洁，优雅，不需要额外的文档工具;
 
 
## 安装
- 如果想在你的TP5项目中使用,那么可以直接使用
```
composer require biyvhudie105/tp5.1-restful
```
- 如果是新项目先要创建tp5.1项目,然后再require

```
composer create-project topthink/think tp5.1
composer require biyvhudie105/tp5.1-restful=dev-master
```
- 如果要使用生成文档 需要在public/static/ 下 安装hadmin
```
cd /public/static/
git clone  https://git.oschina.net/liushoukun/hadmin.git
```
